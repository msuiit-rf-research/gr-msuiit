INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_MSUIIT msuiit)

FIND_PATH(
    MSUIIT_INCLUDE_DIRS
    NAMES msuiit/api.h
    HINTS $ENV{MSUIIT_DIR}/include
        ${PC_MSUIIT_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    MSUIIT_LIBRARIES
    NAMES gnuradio-msuiit
    HINTS $ENV{MSUIIT_DIR}/lib
        ${PC_MSUIIT_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(MSUIIT DEFAULT_MSG MSUIIT_LIBRARIES MSUIIT_INCLUDE_DIRS)
MARK_AS_ADVANCED(MSUIIT_LIBRARIES MSUIIT_INCLUDE_DIRS)

