#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: FM Transmitter
# Author: Daryl P. Pongcol
# GNU Radio version: 3.7.13.5
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from PyQt4.QtCore import QObject, pyqtSlot
from gnuradio import analog
from gnuradio import audio
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio import iio
from gnuradio import qtgui
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import sip
import sys
from gnuradio import qtgui


class fm_tx(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "FM Transmitter")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("FM Transmitter")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "fm_tx")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())


        ##################################################
        # Variables
        ##################################################
        self.wav_file_src_path = wav_file_src_path = '/home/ergwd/Projects/gr-miind/flowgraphs/miind-comms/_sample_audio/franco-a-beautiful-diversion.wav'
        self.samp_rate = samp_rate = 1048576
        self.gain = gain = 50
        self.file_src_volume = file_src_volume = 1
        self.center_freq = center_freq = 24510000000
        self.btn_talk = btn_talk = 0
        self.audio_src_volume = audio_src_volume = 0
        self.audio_rate = audio_rate = 44100

        ##################################################
        # Blocks
        ##################################################
        self.plot_tab_widget = Qt.QTabWidget()
        self.plot_tab_widget_widget_0 = Qt.QWidget()
        self.plot_tab_widget_layout_0 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.plot_tab_widget_widget_0)
        self.plot_tab_widget_grid_layout_0 = Qt.QGridLayout()
        self.plot_tab_widget_layout_0.addLayout(self.plot_tab_widget_grid_layout_0)
        self.plot_tab_widget.addTab(self.plot_tab_widget_widget_0, 'Audio In')
        self.plot_tab_widget_widget_1 = Qt.QWidget()
        self.plot_tab_widget_layout_1 = Qt.QBoxLayout(Qt.QBoxLayout.TopToBottom, self.plot_tab_widget_widget_1)
        self.plot_tab_widget_grid_layout_1 = Qt.QGridLayout()
        self.plot_tab_widget_layout_1.addLayout(self.plot_tab_widget_grid_layout_1)
        self.plot_tab_widget.addTab(self.plot_tab_widget_widget_1, 'FM Out')
        self.top_grid_layout.addWidget(self.plot_tab_widget, 0, 0, 12, 12)
        for r in range(0, 12):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 12):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._center_freq_options = (24510000000, 24520000000, 24530000000, 24540000000, 24550000000, )
        self._center_freq_labels = ('2.451 GHz', '2.452 GHz', '2.453 GHz', '2.453 GHz', '2.453 GHz', )
        self._center_freq_group_box = Qt.QGroupBox('Center Freq')
        self._center_freq_box = Qt.QVBoxLayout()
        class variable_chooser_button_group(Qt.QButtonGroup):
            def __init__(self, parent=None):
                Qt.QButtonGroup.__init__(self, parent)
            @pyqtSlot(int)
            def updateButtonChecked(self, button_id):
                self.button(button_id).setChecked(True)
        self._center_freq_button_group = variable_chooser_button_group()
        self._center_freq_group_box.setLayout(self._center_freq_box)
        for i, label in enumerate(self._center_freq_labels):
        	radio_button = Qt.QRadioButton(label)
        	self._center_freq_box.addWidget(radio_button)
        	self._center_freq_button_group.addButton(radio_button, i)
        self._center_freq_callback = lambda i: Qt.QMetaObject.invokeMethod(self._center_freq_button_group, "updateButtonChecked", Qt.Q_ARG("int", self._center_freq_options.index(i)))
        self._center_freq_callback(self.center_freq)
        self._center_freq_button_group.buttonClicked[int].connect(
        	lambda i: self.set_center_freq(self._center_freq_options[i]))
        self.top_grid_layout.addWidget(self._center_freq_group_box, 1, 13, 1, 2)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._wav_file_src_path_options = ('/home/ergwd/Projects/gr-miind/flowgraphs/miind-comms/_sample_audio/franco-a-beautiful-diversion.wav', '/home/ergwd/Projects/gr-miind/flowgraphs/miind-comms/_sample_audio/franco-a-beautiful-diversion-2.wav', )
        self._wav_file_src_path_labels = ('Franco - A Beautiful Diversion', 'Franco - A Beautiful Diversion 2', )
        self._wav_file_src_path_group_box = Qt.QGroupBox('Songs')
        self._wav_file_src_path_box = Qt.QVBoxLayout()
        class variable_chooser_button_group(Qt.QButtonGroup):
            def __init__(self, parent=None):
                Qt.QButtonGroup.__init__(self, parent)
            @pyqtSlot(int)
            def updateButtonChecked(self, button_id):
                self.button(button_id).setChecked(True)
        self._wav_file_src_path_button_group = variable_chooser_button_group()
        self._wav_file_src_path_group_box.setLayout(self._wav_file_src_path_box)
        for i, label in enumerate(self._wav_file_src_path_labels):
        	radio_button = Qt.QRadioButton(label)
        	self._wav_file_src_path_box.addWidget(radio_button)
        	self._wav_file_src_path_button_group.addButton(radio_button, i)
        self._wav_file_src_path_callback = lambda i: Qt.QMetaObject.invokeMethod(self._wav_file_src_path_button_group, "updateButtonChecked", Qt.Q_ARG("int", self._wav_file_src_path_options.index(i)))
        self._wav_file_src_path_callback(self.wav_file_src_path)
        self._wav_file_src_path_button_group.buttonClicked[int].connect(
        	lambda i: self.set_wav_file_src_path(self._wav_file_src_path_options[i]))
        self.top_grid_layout.addWidget(self._wav_file_src_path_group_box, 0, 13, 1, 2)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.wav_file_src = blocks.wavfile_source(wav_file_src_path, True)
        self.rational_resampler_xxx_0 = filter.rational_resampler_ccc(
                interpolation=30,
                decimation=5,
                taps=None,
                fractional_bw=None,
        )
        self.qtgui_time_sink_x_0_0 = qtgui.time_sink_f(
        	4096, #size
        	audio_rate, #samp_rate
        	"", #name
        	1 #number of inputs
        )
        self.qtgui_time_sink_x_0_0.set_update_time(0.10)
        self.qtgui_time_sink_x_0_0.set_y_axis(-1, 1)

        self.qtgui_time_sink_x_0_0.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_0_0.enable_tags(-1, True)
        self.qtgui_time_sink_x_0_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "")
        self.qtgui_time_sink_x_0_0.enable_autoscale(False)
        self.qtgui_time_sink_x_0_0.enable_grid(False)
        self.qtgui_time_sink_x_0_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0_0.enable_control_panel(False)
        self.qtgui_time_sink_x_0_0.enable_stem_plot(False)

        if not True:
          self.qtgui_time_sink_x_0_0.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qtgui_time_sink_x_0_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_time_sink_x_0_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_0_win = sip.wrapinstance(self.qtgui_time_sink_x_0_0.pyqwidget(), Qt.QWidget)
        self.plot_tab_widget_grid_layout_0.addWidget(self._qtgui_time_sink_x_0_0_win, 0, 0, 6, 12)
        for r in range(0, 6):
            self.plot_tab_widget_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 12):
            self.plot_tab_widget_grid_layout_0.setColumnStretch(c, 1)
        self.qtgui_time_sink_x_0 = qtgui.time_sink_c(
        	4096, #size
        	samp_rate, #samp_rate
        	"", #name
        	1 #number of inputs
        )
        self.qtgui_time_sink_x_0.set_update_time(0.10)
        self.qtgui_time_sink_x_0.set_y_axis(-1, 1)

        self.qtgui_time_sink_x_0.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_0.enable_tags(-1, True)
        self.qtgui_time_sink_x_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "")
        self.qtgui_time_sink_x_0.enable_autoscale(False)
        self.qtgui_time_sink_x_0.enable_grid(False)
        self.qtgui_time_sink_x_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0.enable_control_panel(False)
        self.qtgui_time_sink_x_0.enable_stem_plot(False)

        if not True:
          self.qtgui_time_sink_x_0.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in xrange(2):
            if len(labels[i]) == 0:
                if(i % 2 == 0):
                    self.qtgui_time_sink_x_0.set_line_label(i, "Re{{Data {0}}}".format(i/2))
                else:
                    self.qtgui_time_sink_x_0.set_line_label(i, "Im{{Data {0}}}".format(i/2))
            else:
                self.qtgui_time_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_win = sip.wrapinstance(self.qtgui_time_sink_x_0.pyqwidget(), Qt.QWidget)
        self.plot_tab_widget_grid_layout_1.addWidget(self._qtgui_time_sink_x_0_win, 0, 0, 6, 12)
        for r in range(0, 6):
            self.plot_tab_widget_grid_layout_1.setRowStretch(r, 1)
        for c in range(0, 12):
            self.plot_tab_widget_grid_layout_1.setColumnStretch(c, 1)
        self.qtgui_freq_sink_x_1 = qtgui.freq_sink_c(
        	1024, #size
        	firdes.WIN_BLACKMAN_hARRIS, #wintype
        	0, #fc
        	samp_rate, #bw
        	"", #name
        	1 #number of inputs
        )
        self.qtgui_freq_sink_x_1.set_update_time(0.10)
        self.qtgui_freq_sink_x_1.set_y_axis(-200, 10)
        self.qtgui_freq_sink_x_1.set_y_label('Relative Gain', 'dB')
        self.qtgui_freq_sink_x_1.set_trigger_mode(qtgui.TRIG_MODE_FREE, 0.0, 0, "")
        self.qtgui_freq_sink_x_1.enable_autoscale(False)
        self.qtgui_freq_sink_x_1.enable_grid(False)
        self.qtgui_freq_sink_x_1.set_fft_average(1.0)
        self.qtgui_freq_sink_x_1.enable_axis_labels(True)
        self.qtgui_freq_sink_x_1.enable_control_panel(False)

        if not True:
          self.qtgui_freq_sink_x_1.disable_legend()

        if "complex" == "float" or "complex" == "msg_float":
          self.qtgui_freq_sink_x_1.set_plot_pos_half(not True)

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "dark blue"]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qtgui_freq_sink_x_1.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_freq_sink_x_1.set_line_label(i, labels[i])
            self.qtgui_freq_sink_x_1.set_line_width(i, widths[i])
            self.qtgui_freq_sink_x_1.set_line_color(i, colors[i])
            self.qtgui_freq_sink_x_1.set_line_alpha(i, alphas[i])

        self._qtgui_freq_sink_x_1_win = sip.wrapinstance(self.qtgui_freq_sink_x_1.pyqwidget(), Qt.QWidget)
        self.plot_tab_widget_grid_layout_1.addWidget(self._qtgui_freq_sink_x_1_win, 6, 0, 6, 12)
        for r in range(6, 12):
            self.plot_tab_widget_grid_layout_1.setRowStretch(r, 1)
        for c in range(0, 12):
            self.plot_tab_widget_grid_layout_1.setColumnStretch(c, 1)
        self.qtgui_freq_sink_x_0 = qtgui.freq_sink_f(
        	1024, #size
        	firdes.WIN_BLACKMAN_hARRIS, #wintype
        	0, #fc
        	samp_rate, #bw
        	"", #name
        	1 #number of inputs
        )
        self.qtgui_freq_sink_x_0.set_update_time(0.10)
        self.qtgui_freq_sink_x_0.set_y_axis(-140, 10)
        self.qtgui_freq_sink_x_0.set_y_label('Relative Gain', 'dB')
        self.qtgui_freq_sink_x_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, 0.0, 0, "")
        self.qtgui_freq_sink_x_0.enable_autoscale(False)
        self.qtgui_freq_sink_x_0.enable_grid(False)
        self.qtgui_freq_sink_x_0.set_fft_average(1.0)
        self.qtgui_freq_sink_x_0.enable_axis_labels(True)
        self.qtgui_freq_sink_x_0.enable_control_panel(False)

        if not True:
          self.qtgui_freq_sink_x_0.disable_legend()

        if "float" == "float" or "float" == "msg_float":
          self.qtgui_freq_sink_x_0.set_plot_pos_half(not True)

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "dark blue"]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qtgui_freq_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_freq_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_freq_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_freq_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_freq_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_freq_sink_x_0_win = sip.wrapinstance(self.qtgui_freq_sink_x_0.pyqwidget(), Qt.QWidget)
        self.plot_tab_widget_grid_layout_0.addWidget(self._qtgui_freq_sink_x_0_win, 6, 0, 6, 12)
        for r in range(6, 12):
            self.plot_tab_widget_grid_layout_0.setRowStretch(r, 1)
        for c in range(0, 12):
            self.plot_tab_widget_grid_layout_0.setColumnStretch(c, 1)
        self.pluto_sink_0 = iio.pluto_sink('ip:pluto.local', center_freq, samp_rate, 30000000, 0x8000, False, 0, '', True)
        self.multiply_const_file = blocks.multiply_const_vff((file_src_volume, ))
        self.multiply_const_audio = blocks.multiply_const_vff((audio_src_volume, ))
        _btn_talk_push_button = Qt.QPushButton('Push to Talk')
        self._btn_talk_choices = {'Pressed': 1, 'Released': 0}
        _btn_talk_push_button.pressed.connect(lambda: self.set_btn_talk(self._btn_talk_choices['Pressed']))
        _btn_talk_push_button.released.connect(lambda: self.set_btn_talk(self._btn_talk_choices['Released']))
        self.top_grid_layout.addWidget(_btn_talk_push_button, 12, 13, 1, 2)
        for r in range(12, 13):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(13, 15):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.blocks_multiply_xx_0 = blocks.multiply_vcc(1)
        self.blocks_add_xx_0 = blocks.add_vff(1)
        self.audio_source_0 = audio.source(44100, '', True)
        self.audio_sink_0 = audio.sink(44100, '', True)
        self.analog_wfm_tx_0 = analog.wfm_tx(
        	audio_rate=audio_rate,
        	quad_rate=audio_rate*4,
        	tau=75e-6,
        	max_dev=5e3,
        	fh=-1.0,
        )
        self.analog_sig_source_x_0 = analog.sig_source_c(samp_rate, analog.GR_COS_WAVE, 1000, 1, 0)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_sig_source_x_0, 0), (self.blocks_multiply_xx_0, 1))
        self.connect((self.analog_wfm_tx_0, 0), (self.rational_resampler_xxx_0, 0))
        self.connect((self.audio_source_0, 0), (self.multiply_const_audio, 0))
        self.connect((self.blocks_add_xx_0, 0), (self.analog_wfm_tx_0, 0))
        self.connect((self.blocks_add_xx_0, 0), (self.audio_sink_0, 0))
        self.connect((self.blocks_add_xx_0, 0), (self.qtgui_freq_sink_x_0, 0))
        self.connect((self.blocks_add_xx_0, 0), (self.qtgui_time_sink_x_0_0, 0))
        self.connect((self.blocks_multiply_xx_0, 0), (self.pluto_sink_0, 0))
        self.connect((self.blocks_multiply_xx_0, 0), (self.qtgui_freq_sink_x_1, 0))
        self.connect((self.blocks_multiply_xx_0, 0), (self.qtgui_time_sink_x_0, 0))
        self.connect((self.multiply_const_audio, 0), (self.blocks_add_xx_0, 0))
        self.connect((self.multiply_const_file, 0), (self.blocks_add_xx_0, 1))
        self.connect((self.rational_resampler_xxx_0, 0), (self.blocks_multiply_xx_0, 0))
        self.connect((self.wav_file_src, 0), (self.multiply_const_file, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "fm_tx")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_wav_file_src_path(self):
        return self.wav_file_src_path

    def set_wav_file_src_path(self, wav_file_src_path):
        self.wav_file_src_path = wav_file_src_path
        self._wav_file_src_path_callback(self.wav_file_src_path)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.qtgui_time_sink_x_0.set_samp_rate(self.samp_rate)
        self.qtgui_freq_sink_x_1.set_frequency_range(0, self.samp_rate)
        self.qtgui_freq_sink_x_0.set_frequency_range(0, self.samp_rate)
        self.pluto_sink_0.set_params(self.center_freq, self.samp_rate, 30000000, 0, '', True)
        self.analog_sig_source_x_0.set_sampling_freq(self.samp_rate)

    def get_gain(self):
        return self.gain

    def set_gain(self, gain):
        self.gain = gain

    def get_file_src_volume(self):
        return self.file_src_volume

    def set_file_src_volume(self, file_src_volume):
        self.file_src_volume = file_src_volume
        self.multiply_const_file.set_k((self.file_src_volume, ))

    def get_center_freq(self):
        return self.center_freq

    def set_center_freq(self, center_freq):
        self.center_freq = center_freq
        self._center_freq_callback(self.center_freq)
        self.pluto_sink_0.set_params(self.center_freq, self.samp_rate, 30000000, 0, '', True)

    def get_btn_talk(self):
        return self.btn_talk

    def set_btn_talk(self, btn_talk):
        self.btn_talk = btn_talk

    def get_audio_src_volume(self):
        return self.audio_src_volume

    def set_audio_src_volume(self, audio_src_volume):
        self.audio_src_volume = audio_src_volume
        self.multiply_const_audio.set_k((self.audio_src_volume, ))

    def get_audio_rate(self):
        return self.audio_rate

    def set_audio_rate(self, audio_rate):
        self.audio_rate = audio_rate
        self.qtgui_time_sink_x_0_0.set_samp_rate(self.audio_rate)


def main(top_block_cls=fm_tx, options=None):

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
