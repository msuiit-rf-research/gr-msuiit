#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
# SPDX-License-Identifier: GPL-3.0
#
##################################################
# GNU Radio Python Flow Graph
# Title: miind | ISDBT Obtain Parameters
# Generated: Fri Nov  2 22:02:39 2018
# GNU Radio version: 3.7.12.0
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from gnuradio import gr
from gnuradio import qtgui

from fm_tx import fm_tx
# from mixins import mixin_buttons, mixin_params


class main_fm_tx(fm_tx):

    def __init__(self):
        fm_tx.__init__(self)

    def set_wav_file_src_path(self, wav_file_src_path):
        fm_tx.set_wav_file_src_path(self, wav_file_src_path)
        self.wav_file_src.close(self)
        self.wav_file_src.open(self, wav_file_src_path, True)

    def set_btn_talk(self, btn_talk):
        print "btn_talk callback"
        if btn_talk:
            self.set_audio_src_volume(1)
            self.set_file_src_volume(0.2)
        else:
            self.set_audio_src_volume(0)
            self.set_file_src_volume(1)


def main(top_block_cls=main_fm_tx, options=None):
    if gr.enable_realtime_scheduling() != gr.RT_OK:
        print "Error: failed to enable real-time scheduling."

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()